#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <StavrosUtils.h>
#include "epd_driver.h"
#include "esp_adc_cal.h"
#include "font/firasans.h"
#include "splash.h"
#include "batterylow.h"
#include "pins.h"

// The T5's buttons are: RST, 0, 35, 34, 39
#define REPAIR_BUTTON 35
#define FACTORY_RESET_BUTTON 34
#define SECOND_IMAGE_SWITCH GPIO_NUM_39
#define LED_PIN 5

StavrosUtils utils;
char *framebuffer;

WiFiClientSecure client;

void storeEmptyHash() {
    strncpy(utils.state.hash, "\0", 128);
    utils.saveState();
}

void showSplashScreen() {
    epd_copy_to_framebuffer(epd_full_screen(), (uint8_t *)splash_data, (uint8_t *)framebuffer);
    displayFramebuffer();
    storeEmptyHash();
}

void showBatteryLowScreen() {
    epd_copy_to_framebuffer(epd_full_screen(), (uint8_t *)batterylow_data, (uint8_t *)framebuffer);
    displayFramebuffer();

    utils.debug("Battery is too low, disabling timer.");
    digitalWrite(LED_PIN, HIGH);

    // This function should never return.
    deepSleep(false);
}

void configModeCallback(WiFiManager *myWiFiManager) {
    showSplashScreen();
}


void print_wakeup_reason() {
    esp_sleep_wakeup_cause_t wakeup_reason;

    wakeup_reason = esp_sleep_get_wakeup_cause();

    switch (wakeup_reason) {
    case ESP_SLEEP_WAKEUP_EXT0 :
        utils.debug("Wakeup by external signal using RTC_IO.");
        break;
    case ESP_SLEEP_WAKEUP_EXT1 :
        utils.debug("Wakeup by external signal using RTC_CNTL.");
        break;
    case ESP_SLEEP_WAKEUP_TIMER :
        utils.debug("Wakeup by timer.");
        break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD :
        utils.debug("Wakeup by touchpad.");
        break;
    case ESP_SLEEP_WAKEUP_ULP :
        utils.debug("Wakeup by ULP program.");
        break;
    default :
        utils.debug("Wakeup was not caused by deep sleep.");
        break;
    }
}


void deepSleep(bool enableWakeup) {
    if (enableWakeup) {
        utils.debug("Sleeping with wakeup...");
        esp_sleep_enable_timer_wakeup(std::stoi(REFRESH_MINUTES) * 60 * 1000 * 1000);
        esp_sleep_enable_ext0_wakeup(SECOND_IMAGE_SWITCH, 0);
    } else {
        utils.debug("Sleeping without wakeup...");
        esp_sleep_disable_wakeup_source(ESP_SLEEP_WAKEUP_ALL);
    }
    delay(100);
    esp_deep_sleep_start();
}


float readBatteryVoltage() {
    int vref = 1100;
    char buf[256];

    epd_poweron();
    delay(50);

    // Calibrate vref.
    esp_adc_cal_characteristics_t adc_chars;
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars);
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        vref = adc_chars.vref;
    }

    // Improve ADC measurement accuracy.
    delay(10);
    uint16_t v = analogRead(BATT_PIN);
    float battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (vref / 1000.0);

    epd_poweroff();
    delay(50);

    return battery_voltage;
}


void initializeFramebuffer() {
    framebuffer = (char *)heap_caps_malloc(EPD_WIDTH * EPD_HEIGHT / 2, MALLOC_CAP_SPIRAM);
    memset(framebuffer, 0xFF, EPD_WIDTH * EPD_HEIGHT / 2);
}


bool downloadFile(const char *url, char *buffer) {
    char http_buffer[255];

    // Copy the URL into a new buffer so we can modify it.
    char *url_copy = (char *)heap_caps_malloc(strlen(url) + 1, MALLOC_CAP_SPIRAM);
    strcpy(url_copy, url);

    utils.debug(String("Downloading ") + String(url));

    // Split the url into host and path
    char *host = strtok(url_copy, "/");
    char *path = strtok(NULL, "");

    // Security, what is that.
    client.setInsecure();

    if (!client.connect(host, 443)) {
        utils.debug("Connection failed.");
        return false;
    } else {
        utils.debug("Connected to HTTP server.");
        utils.debug("Fetching " + String(url) + "...");

        sprintf(http_buffer, "GET /%s HTTP/1.1", path);
        client.println(http_buffer);
        sprintf(http_buffer, "Host: %s", host);
        client.println(http_buffer);
        client.println("User-Agent: iFrame");
        client.println("Connection: close");
        client.println();

        while (client.connected()) {
            String line = client.readStringUntil('\n');
            if (line == "\r") {
                utils.debug("Got headers.");
                break;
            }
        }

        int counter = 0;
        while (client.connected()) {
            if (client.available()) {
                buffer[counter++] = client.read();
            }
        }
        buffer[counter] = 0;
        utils.debug(String(counter));

        client.stop();
    }
    return true;
}


void displayFramebuffer() {
    epd_poweron();
    // Two cycles were enough to clear, and took less time, so 2 it is. The cycle time
    // of 12 was the default
    epd_clear_area_cycles(epd_full_screen(), 2, 12);
    volatile uint32_t t1 = millis();
    epd_draw_grayscale_image(epd_full_screen(), (uint8_t *)framebuffer);
    volatile uint32_t t2 = millis();
    Serial.printf("EPD draw took %dms.\r\n", t2 - t1);
    epd_poweroff();

    delay(500);
}


void downloadAndShowImage() {
    bool downloadSuccess;

    if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT0) {
        downloadSuccess = downloadFile(utils.state.imageUrl2, framebuffer);
    } else {
        downloadSuccess = downloadFile(utils.state.imageUrl, framebuffer);
    }

    if (downloadSuccess) {
        displayFramebuffer();
    }

}

void repairScreen() {
    int32_t i = 0;

    Rect_t area = epd_full_screen();
    epd_poweron();
    delay(10);
    epd_clear();
    for (i = 0; i < 20; i++) {
        epd_push_pixels(area, 50, 0);
        delay(500);
    }
    epd_clear();
    for (i = 0; i < 40; i++) {
        epd_push_pixels(area, 50, 1);
        delay(500);
    }
    epd_clear();
    epd_poweroff_all();
}


bool checkForNewImage() {
    char sha_buffer[128];

    utils.loadState();

    // This if check could be cleaner, but meh.
    if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_EXT0) {
        downloadFile((String(utils.state.imageUrl2) + ".sha").c_str(), sha_buffer);
    } else {
        downloadFile((String(utils.state.imageUrl) + ".sha").c_str(), sha_buffer);
    }

    utils.debug("Stored hash: " + String(utils.state.hash));
    utils.debug("Server hash: " + String(sha_buffer));

    if (strncmp(utils.state.hash, sha_buffer, 64) == 0) {
        utils.debug("No change since last display, exiting.");
        return false;
    }

    if (strlen(sha_buffer) == 64) {
        // This is a valid hash, and not a 404 or whatever.
        utils.debug("Saving state...");
        strncpy(utils.state.hash, sha_buffer, 128);
        utils.saveState();
    }
    utils.debug("There is a new image.");
    return true;
}

void setup() {
    pinMode(REPAIR_BUTTON, INPUT);
    pinMode(FACTORY_RESET_BUTTON, INPUT);
    pinMode(LED_PIN, OUTPUT);

    Serial.begin(115200);

    if (digitalRead(FACTORY_RESET_BUTTON) == LOW) {
        utils.debug("Resetting to factory settings...");
        utils.wifiManager.resetSettings();
    }

    initializeFramebuffer();
    epd_init();

    utils.wifiManager.setAPCallback(configModeCallback);
    utils.connectToWiFi(5 * 60);

    if (WiFi.status() != WL_CONNECTED) {
        deepSleep(false);
    }

    print_wakeup_reason();

    digitalWrite(LED_PIN, LOW);

}


void loop() {
    float v = readBatteryVoltage();
    utils.debug("Voltage: " + String(v) + " V");

    if (v < 3.3) {
        // The battery is too low, don't wake up regularly.
        storeEmptyHash();
        showBatteryLowScreen();
    }

    if (checkForNewImage()) {
        downloadAndShowImage();
    }

    if (digitalRead(REPAIR_BUTTON) == LOW) {
        utils.debug("Repairing screen...");
        repairScreen();
        downloadAndShowImage();
    }

    utils.debug("Sleeping...");
    digitalWrite(LED_PIN, HIGH);
    deepSleep(true);
}
