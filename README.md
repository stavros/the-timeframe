The Timeframe
=============

![](misc/timeframe.jpg)

The Timeframe is an e-ink frame that displays my daily calendar. It can display
basically any image, but that's what I use it for!  To make it, I used a LilyGo T5 4.7"
display, and a bunch of software.


How it works
------------

At its essence, all the Timeframe does is display images. It wakes up, talks to a web
server you tell it to (over HTTPS), and downloads and displays an image. After this is
finished, it goes into deep sleep (basically turns off) for half an hour, to save
battery.

After half an hour, the process begins again, but only if the file on the server has
changed. The Timeframe knows this because of a separate file that sits next to the image
file and uniquely identifies the contents of the image file.

This repository contains a helper script that will convert any image to the appropriate
format and dimensions, along with some postprocessing to make the image display better..


Parts
-----

To make this, you need:

* A LilyGo T5, from [AliExpress](https://www.aliexpress.com/item/1005002272417292.html)
  or [Amazon](https://www.amazon.com/dp/B09FSRLWMD/) (non-affiliate links because I
  can't be assed setting up accounts).
* A 18650 battery (Sony VTC-6 recommended). You can power it from USB as well, if you
  don't want a battery. The battery should last tens of days at half-hour refresh
  intervals, though I haven't really measured, and it recharges from USB.
* A 3D printer, if you want to 3D-print [the case I made for it](https://www.printables.com/model/416409-the-timeframe).
* Some sort of server to generate the images for the Timeframe to display (I can be
  convinced to set one up, if you want to have some generic images).
* Infinite time to gaze in wonder at the beauty that is the Timeframe (you might miss
  all your meetings).

Congratulations, you are now the proud owner of a Timeframe.


Installation
------------

To install the software onto your LilyGo T5:

* Download [the latest build](https://gitlab.com/stavros/the-timeframe/-/jobs/artifacts/master/download?job=test).
* Flash it to the LilyGo T5 somehow (probably with `esptool.py --chip=esp32 timeframe.bin`, but I'm not sure).

Or, you can build the firmware yourself:

* Install [PlatformIO](https://platformio.org).
* Edit `.config` to change any parameters you might want to.
* Connect the LilyGo T5 to your computer via the USB cable.
* Run `./build usb`.

When Timeframe has been installed on the T5, connect to the "Timeframe" WiFi network to
configure the device.  It will ask you for WiFi credentials (so it can download the
image), along with the URL where the image will live.

The URL should be entered without the protocol (it's always HTTPS without certificate
verification). For example, `example.com/timeframe/image`.

You can also enter a URL to a second image, which can be displayed with a button press.


Usage
-----

To convert any image to the format necessary for display on the Timeframe, you can use
the `convert_image.py` script in the `scripts` directory.

If you're displaying photos, I recommend you use the `--dither` option, as that makes
them look much better. There's also a `--brightness` option that lets you brighten the
image a bit, as the e-ink screen tends to darken it.


Buttons
-------

Here's what the Timeframe's buttons do, from top to bottom:

### First button: Reset

The reset button starts the display up, or resets it if it's already working. Nothing
much special here, this button will always start things up. The Timeframe will try to
look for a new image, and it will show it if one exists, otherwise it will go back to
sleep and wake up 30 minutes later and repeat this process.

### Second button: Dragons

Just do yourself a favor and never press this. I'd redesign the case so this button
isn't there, if I knew precisely what the hell it does except screw things up.

### Third button: Repair

If you hold this button down shortly after boot, you will enter "repair" mode, which
tries to repair ghosting on the screen by flashing it black and white repeatedly. You
shouldn't need this, but if you do, here it is.

### Fourth button: Factory reset

Do you like that? "Factory"? I called it that because it's most apt to what it does.
If you hold this button down while pressing the reset button, it'll delete your WiFi
settings from the Timeframe, allowing you to configure it again.

### Fifth button: Second image

Pressing this button will make the Timeframe look for the second image (the one you
defined in "Second image URL" when setting it up). It doesn't change anything else, this
is a one-off. When the Timeframe refreshes periodically, it will still try to download
and show the first image.

I use this for showing my guests a QR code for connecting to my wifi, but you can use it
for whatever you want to show once. Go nuts.
