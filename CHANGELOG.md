# Changelog


## Unreleased

### Features

* Don't wake up on a timer if there's no WiFi. [Stavros Korokithakis]

* Calibrate the reference voltage before measurement. [Stavros Korokithakis]

* Show a "battery low" screen if... the battery is low. [Stavros Korokithakis]

* Allow showing a second image. [Stavros Korokithakis]

* Use fewer cycles to clear the screen, to make it less distracting. [Stavros Korokithakis]

* Configure the image URL in the WiFi portal. [Stavros Korokithakis]

* Increase interval before capture. [Stavros Korokithakis]

* Make the Google Calendar view a bit larger. [Stavros Korokithakis]

### Fixes

* Store the empty hash on battery low. [Stavros Korokithakis]

* Improve login detection. [Stavros Korokithakis]

* Clear hash if we're showing the splash screen. [Stavros Korokithakis]

* Get headless GCal capturing working. [Stavros Korokithakis]


