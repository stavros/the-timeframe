#include <EEPROM.h>
#include <DNSServer.h>
#include "WiFiManager.h"
#include <WiFiUdp.h>

#define CONFIG_VERSION 1875674

typedef struct {
    unsigned long int version;
    char hash[128] ;
    char imageUrl[256] ;
    char imageUrl2[256] ;
} StateType;


class StavrosUtils {
private:
    IPAddress broadcastIP;


    void _connectToWiFi(int timeout) {
        debug("Connecting to WiFi...");

        this->loadState();

        WiFiManagerParameter custom_image_url("imageurl", "Image URL", this->state.imageUrl, 255);
        wifiManager.addParameter(&custom_image_url);

        WiFiManagerParameter custom_image_url_2("imageurl2", "Second image URL", this->state.imageUrl2, 255);
        wifiManager.addParameter(&custom_image_url_2);

        wifiManager.setMinimumSignalQuality(10);

        if (timeout > 0) {
            wifiManager.setConfigPortalTimeout(timeout);
        }
        wifiManager.setHostname(PROJECT_NAME);
        wifiManager.autoConnect(PROJECT_NAME);
        this->broadcastIP = WiFi.localIP();
        this->broadcastIP[3] = 255;

        strncpy(this->state.imageUrl, custom_image_url.getValue(), 256);
        strncpy(this->state.imageUrl2, custom_image_url_2.getValue(), 256);
        this->saveState();

        debug("Connected.");
    }

public:
    WiFiManager wifiManager;
    StateType state = {};

    void loadState() {
        EEPROM.begin(sizeof(this->state));
        EEPROM.get(0, this->state);
        if (this->state.version != CONFIG_VERSION) {
            this->state.version = CONFIG_VERSION;
            strcpy(this->state.hash, "\0");
            strcpy(this->state.imageUrl, "pub-7248391c797943d89288b13a4c4bc7d5.r2.dev/timeframe/image\0");
            strcpy(this->state.imageUrl2, "pub-7248391c797943d89288b13a4c4bc7d5.r2.dev/timeframe/image\0");
        }
    }

    void saveState() {
        EEPROM.begin(sizeof(this->state));
        EEPROM.put(0, this->state);
        EEPROM.commit();
    }

    void logUDP(String message) {
        if (WiFi.status() != WL_CONNECTED) {
            return;
        }

        WiFiUDP Udp;

        // Listen with `nc -kul 37243`.
        Udp.beginPacket(this->broadcastIP, 37243);
        Udp.print(("(" PROJECT_NAME  ": " + String(millis()) + " - " + WiFi.localIP().toString() + ") " + ": " + message + "\n").c_str());
        Udp.endPacket();
    }

    void debug(String text) {
        Serial.println(text);
        logUDP(text);
    }

    void connectToWiFi() {
        _connectToWiFi(0);
    }


    void connectToWiFi(int timeout) {
        _connectToWiFi(timeout);
    }
};
